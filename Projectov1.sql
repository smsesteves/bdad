.mode columns 
.headers on


DROP TABLE Modelo;
DROP TABLE Marca;
DROP TABLE Aviao;
DROP TABLE Rota;
DROP TABLE Catering;
DROP TABLE Aeroporto;
DROP TABLE Empregado;
DROP TABLE Cidade;
DROP TABLE Pais;
DROP TABLE Cliente;
DROP TABLE Funcao;
DROP TABLE Voo;
DROP TABLE VooCliente;
DROP TABLE VooEmpregado;


CREATE TABLE Catering
(	
	id 			INTEGER PRIMARY KEY AUTOINCREMENT,
	nome		CHAR(20)  NOT NULL,
	menu		TEXT,
	NIF			INT
);

CREATE TABLE Pais
(	
	id 			INTEGER PRIMARY KEY  AUTOINCREMENT,
	nome		CHAR(20)  NOT NULL,
	lingua		CHAR(10)
);

CREATE TABLE Cidade
(	
	id 			INTEGER PRIMARY KEY  AUTOINCREMENT,
	nome		CHAR(20)  NOT NULL,
	pais		INT REFERENCES Pais(id)
);

CREATE TABLE Aeroporto
(	
	id 			INTEGER PRIMARY KEY  AUTOINCREMENT,
	nome		CHAR(200),
	sigla		CHAR(6)  NOT NULL,
	morada		TEXT,
	cidade		INT REFERENCES Cidade(id)
);

CREATE TABLE Marca
(	
	id 			INTEGER PRIMARY KEY  AUTOINCREMENT,
	nome 		CHAR(20),
	sigla 		CHAR(6)  NOT NULL,
	contacto	TEXT
);

CREATE TABLE Modelo
(	
	id 			INTEGER PRIMARY KEY  AUTOINCREMENT,
	nome 		CHAR(20),
    lotacao     INT,
	marca 		INT REFERENCES Marca(id)
);

CREATE TABLE Aviao
(	
	id 			INTEGER PRIMARY KEY  AUTOINCREMENT,
	activo 		BOOLEAN,
	dataCompra 	DATE  NOT NULL,	
	modelo 		INT REFERENCES Modelo(id)
);

CREATE TABLE Rota
(	
	id 			INTEGER PRIMARY KEY  AUTOINCREMENT,
	precoBase	REAL,
	nome		CHAR(20),
	dataCriacao	DATE,
	catering	INT REFERENCES Catering(id),
	aeroporto1	INT REFERENCES Aeroporto(id),
	aeroporto2  INT REFERENCES Aeroporto(id)
	CONSTRAINT ch_a1_a2 CHECK (aeroporto1!=aeroporto2)
);

CREATE TABLE Cliente
(	
	id 			INTEGER PRIMARY KEY  AUTOINCREMENT,
	nome		CHAR(20)  NOT NULL,
	BI			INT  NOT NULL UNIQUE,
	NIF			INT  NOT NULL UNIQUE,
	sexo		INT,
	dataNasc	DATE  NOT NULL,
	pais		INT REFERENCES Pais(id)
);

CREATE TABLE Voo
(	
	id 			INTEGER PRIMARY KEY  AUTOINCREMENT,
	partida		DATETIME NOT NULL,
    chegada     DATETIME NOT NULL,
	direcao		INT,
	aviao		INT REFERENCES Aviao(id),
	rota		INT REFERENCES Rota(id),
    CONSTRAINT ch_d1_d2 CHECK (partida<chegada),
    CONSTRAINT dr CHECK (direcao<2 and direcao > -1)
);

CREATE TABLE VooCliente
(	
	voo 		INT REFERENCES Voo(id) ,
	cliente	    INT REFERENCES Cliente(id),
	PRIMARY KEY (voo, cliente)
);

CREATE TABLE Funcao
(	
	id 			INTEGER PRIMARY KEY  AUTOINCREMENT,
	nome		CHAR(20) NOT NULL,
	salarioHora	REAL
);

CREATE TABLE Empregado
(	
	id 			INTEGER PRIMARY KEY AUTOINCREMENT,
	nome		CHAR(20) NOT NULL,
	morada		TEXT,
	sexo		INT,
	BI			INT NOT NULL UNIQUE,
	NIF			INT NOT NULL UNIQUE,
	funcao		INT REFERENCES Funcao(id)
);

CREATE TABLE VooEmpregado
(
    empregado   INT REFERENCES Empregado(id),
    rota        INT REFERENCES Rota(id),
    PRIMARY KEY (empregado, rota)
);

INSERT INTO Funcao (nome, salarioHora) VALUES ("Pilot", 50);
INSERT INTO Funcao (nome, salarioHora) VALUES ("Host", 10);

INSERT INTO Empregado (nome, morada, sexo, BI, NIF, funcao) VALUES ("Joao Silva", "Rua X", 1, 14578965, 25412578, 1);
INSERT INTO Empregado (nome, morada, sexo, BI, NIF, funcao) VALUES ("Jose Silva", "Rua A", 1, 12547858, 32541587, 1);
INSERT INTO Empregado (nome, morada, sexo, BI, NIF, funcao) VALUES ("Josefina", "Rua Y", 0, 14578966, 25412571, 2);

INSERT INTO Pais (nome, lingua) VALUES ("Portugal", "Portuguese");
INSERT INTO Pais (nome, lingua) VALUES ("Spain", "Spanish");
INSERT INTO Pais (nome, lingua) VALUES ("France", "French");

INSERT INTO Cidade (nome, pais) VALUES ("Porto", 1);
INSERT INTO Cidade (nome, pais) VALUES ("Lisboa", 1);
INSERT INTO Cidade (nome, pais) VALUES ("Faro", 1);
INSERT INTO Cidade (nome, pais) VALUES ("Barcelona", 2);
INSERT INTO Cidade (nome, pais) VALUES ("Madrid", 2);
INSERT INTO Cidade (nome, pais) VALUES ("Paris", 3);

INSERT INTO Cliente (nome, BI, NIF, sexo, dataNasc, pais) VALUES ("Andre", 12978757, 124578, 1, "23/4/1993", 1);
INSERT INTO Cliente (nome, BI, NIF, sexo, dataNasc, pais) VALUES ("Esteves", 12978767, 324578, 1, "1/2/1993", 2);
INSERT INTO Cliente (nome, BI, NIF, sexo, dataNasc, pais) VALUES ("Marias", 12945767, 323278, 1, "1/3/1993", 1);

INSERT INTO Aeroporto (nome, sigla, morada, cidade) VALUES ("Aeroporto Sa Carneiro", "OPO", "Rua Sa Carneiro", 1);
INSERT INTO Aeroporto (nome, sigla, morada, cidade) VALUES ("Aeroporto da Portela", "LIS", "Rua da Portela", 2);
INSERT INTO Aeroporto (nome, sigla, morada, cidade) VALUES ("Aeroporto de Barcelona", "BRC", "Rua de Barcelona", 4);

INSERT INTO Catering (nome, menu, NIF) VALUES ("ITAU", "Gourmet", 21548546);

INSERT INTO Rota (precoBase, nome, dataCriacao, catering, aeroporto1, aeroporto2) VALUES (50, "Porto - Lisboa", "29/4/2013", 1, 1, 2);
INSERT INTO Rota (precoBase, nome, dataCriacao, catering, aeroporto1, aeroporto2) VALUES (150, "Porto - Barcelona", "29/4/2013", NULL, 1, 3);

INSERT INTO Marca (nome, sigla, contacto) VALUES ("Boeing", "BOE", "+351 912603763");
INSERT INTO Marca (nome, sigla, contacto) VALUES ("Airbus", "AIB", "+351 912603764");

INSERT INTO Modelo (nome, marca, lotacao) VALUES ("B737", 250, 1);
INSERT INTO Modelo (nome, marca, lotacao) VALUES ("A747", 255, 2);

INSERT INTO Aviao (activo, dataCompra, modelo) VALUES (1, "29/4/2013", 1);
INSERT INTO Aviao (activo, dataCompra, modelo) VALUES (1, "29/4/2013", 2);
INSERT INTO Aviao (activo, dataCompra, modelo) VALUES (0, "29/4/2013", 2);

INSERT INTO Voo (partida, chegada, direcao, aviao, rota) VALUES ("29/4/2013 16:39:00", "29/4/2013 17:39:00", 0, 1, 1);
INSERT INTO Voo (partida, chegada, direcao, aviao, rota) VALUES ("29/4/2013 16:39:00", "29/4/2013 17:39:00", 1, 1, 1);
INSERT INTO Voo (partida, chegada, direcao, aviao, rota) VALUES ("29/4/2013 16:39:00", "29/4/2013 19:39:00", 0, 2, 2);
INSERT INTO Voo (partida, chegada, direcao, aviao, rota) VALUES ("29/4/2013 16:39:00", "29/4/2013 19:39:00", 1, 2, 2);

INSERT INTO VooCliente (voo, cliente) VALUES (1, 1);
INSERT INTO VooCliente (voo, cliente) VALUES (1, 2);
INSERT INTO VooCliente (voo, cliente) VALUES (2, 3);

INSERT INTO VooEmpregado (empregado, rota) VALUES (1, 2);
INSERT INTO VooEmpregado (empregado, rota) VALUES (1, 1);
INSERT INTO VooEmpregado (empregado, rota) VALUES (2, 1);